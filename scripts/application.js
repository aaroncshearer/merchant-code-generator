$(document).ready(function() {
    /******************** General Functions *************/
    aver = '0.1.2'
    $('#cg-version .application').text(aver);
    $('#cg-version .template').text(tver);

    resetVars = function() {
        window.numElements = 0;
        elArr = {};
        pageEdited = false;
        $('#element-list,#new-element-area,#edit-area,#preview-pane .preview, #edit-area').html('');
    }
    addFocus = function(thisEl) {
        $('#'+thisEl).focus(function() {
            setInactive($(this).attr('id'));
        }).click(function(){
            setInactive($(this).attr('id'));
        });
    }
    setInactive = function(thisEl) {
        //Check if we need to save
        if ($('#edit-area').hasClass('edited')) {
            if (confirm('The current template has been edited. Do you want to save?')) {
                editSave(false);
            } else {
                $('#edit-title').focus();
                return false;
            }
        }

        if ($('#'+thisEl).hasClass('element-link') && $('#'+thisEl).hasClass('inactive')) {
            editElement(thisEl);
        } 
        setActiveLeftNav(thisEl);
    }

    setActiveLeftNav = function(thisEl) {
        if (thisEl == 'new-element-selector') {
            $('#edit-area').removeClass('edited');
            $('#edit-pane').fadeOut();
        }

        //Unsetting active
        $('.element-link, #new-page-selector, .container').addClass('inactive').removeClass('childactive').removeClass('active').removeClass('currentlevel');

        //Setting Active
        $('#'+thisEl).removeClass('inactive').addClass('active');
        $('#'+thisEl).parents('.container').addClass('childactive').removeClass('inactive');
        $('#'+thisEl).parent().addClass('active').removeClass('childactive');
    }

    /******************** Populate Pages ****************/
    pString = '<select id="new-page-selector"><option class="remove" selected="true" label value>Select Page</option><option class="remove" disabled="true">_____________________</option>';
    for (var key in myPages) {
        if (myPages[key]['name'])
            pString += '<option value="'+key+'">'+myPages[key]['name']+'</option>';
    }
    pString += '</select>';
    $('#page-type').append(pString);

    window.pageType = false;
    $('#new-page-selector').change(function() {
        if (window.pageType==false) {
            $(this).addClass('inactive');
            $(this).find('.remove').remove();
            setPageType();
        } else {
            if (window.numElements==0) {
                setPageType();
            } else {
                if (confirm('Are you sure you want to switch page types?')) {
                    setPageType();
                } else {
                    $('#new-page-selector option[value="'+window.pageType+'"]')[0].selected = true;
                }
            }
        }
    });

    setPageType = function() {
        window.pageType = $('#new-page-selector').val();
        window.pageTitle = $('#new-page-selector option[value="'+window.pageType+'"]')[0].text;
        $(document)[0].title = window.pageTitle + ' | Express Code Generator';

        resetVars();
        popTemplates();
    }

    /****************** Populate Templates ************/
    window.numElements = 0;
    popTemplates = function() {
        pString = '<div id="new-element-selector-area"><select id="new-element-selector"><option class="remove" selected="true" label value>New Element Template</option><option class="remove" disabled="true">_____________________</option>';
        
        // Check for shared templates
        if (moreTemplates = myPages[window.pageType]['moreTemplates']) {
            moreTemplates = moreTemplates.split(',');
            for (var m in moreTemplates) {
                m = moreTemplates[m];
                for (var key in myPages[m]['templates']) {
                    if (
                        (myPages[m]['templates'][key]['requiresparent'] != true) //If this is ONLY a child element
                            &&
                        ((myPages[m]['templates'][key]['debug']!=true) || (myPages[m]['templates'][key]['debug']==true && window.location.href.match('debug')))
                       ) {
                        pString += '<option data-template="'+m+'" value="'+key+'">'+myPages[m]['templates'][key]['name']+'</option>';
                    }
                }    
            }
        }

        // Check for templates unique to this page type
        if (myPages[window.pageType]['templates']) {
            for (var key in myPages[window.pageType]['templates']) {
                if (myPages[m]['templates'][key]['requiresparent'] != true) {
                    pString += '<option data-template="'+window.pageType+'" value="'+key+'">'+myPages[window.pageType]['templates'][key]['name']+'</option>';
                }
            }
        }

        pString += '</select><a data-target="new-element-selector" class="addbutton" href="javascript:">Add Element</a></div>';
        $('#new-element-area').append(pString);
        addFocus('new-element-selector');

        $('#new-element-selector').change(function() {
            $(this).addClass('add');
            //$(this).removeClass('inactive');
            $(this).find('.remove').remove();
        }).focus(function() {
            if ($(this).val()!='') {
                $(this).addClass('add');
            }
            setInactive($(this).attr('id'));
        });

        $('.addbutton').click(function() {
            addEl();
        });
    }

    addEl = function(el) {
        //If we are adding this as a child
        if (el) {
            params = el.split('_');
            
            tID = 'el-'+window.numElements; //Element ID
            
            tval = params[2]; //Template value
            ttext = $('#'+params[0]+'_'+params[1]+'_'+params[2]+' .text').text();
            ttemplate = params[1]; //Template Page Type
            parentEl = params[0];

            inserttarget = $('#'+params[0]).parent().find('.children:first'); //Where to insert the new Element
        } else {
            tID = 'el-'+window.numElements; //Element ID
            
            tval = $('#new-element-selector').val(); //Template value
            ttext = $('#new-element-selector option[value="'+tval+'"]').text(); //Template Title
            ttemplate = $('#new-element-selector option[value="'+tval+'"]').attr('data-template'); //Template Page Type

            parentEl = false;

            inserttarget = $('#element-list'); //Where to insert the new Element
        }

        hasChildren = (myPages[ttemplate]['templates'][tval]['children'] ? true : false);
        tString = '<div class="element-link'+(hasChildren?' pre':'')+'" '+(parentEl?'data-parent="'+parentEl+'"':'')+' data-template="'+ttemplate+'" data-type="'+tval+'" id="'+tID+'">'+ttext+'</div>';

        if (hasChildren) {
            tString = '<div class="container">'+tString+'<div class="children"></div><div class="add-child">';
            for (i=0;i<myPages[ttemplate]['templates'][tval]['children'].length;i++) {
                keys = myPages[ttemplate]['templates'][tval]['children'][i].split('-');
                tString += '<div id="'+tID+'_'+keys[0]+'_'+keys[1]+'" data-template="'+keys[0]+'" data-type="'+keys[1]+'" class="add-sub-element-link">Add <span class="text">'+myPages[keys[0]]['templates'][keys[1]]['name']+'</span></div>';
            }

            window.numElements++;
            postID = 'el-'+window.numElements;
            tString += '</div><div class="element-link post" data-template="'+ttemplate+'" data-type="'+tval+'" id="'+postID+'">'+ttext+'</div></div>';
        }

        inserttarget.append(tString);
        addFocus(tID);
        
        //Add Subelement Clickers
        $('#'+tID).parent().find('.add-sub-element-link').click(function() {
            addEl($(this).attr('id'));
        });

        $('#'+tID).parent().find('.children').sortable({ distance: 15, update: function() {generateHTML();} });

        $('#'+tID).dblclick(function() {
            if (ren = prompt("Rename Template\r\n\r\nEnter New Template Name:")) {
                $(this).text(ren);
                $('#edit-title').val(ren);
                editSave(false);
            }
        });
        
        if (editElement(tID)) {
            $('#'+tID).focus();    
        }

        setInactive(tID);

        window.numElements++; //This is just to stagger the ids

        if (window.numElements>1) {
            $("#element-list").sortable({ distance: 15, update: function() {generateHTML();} });
        }
    }
    /***************** Edit Template *********************/
    
    prepareElement = function(thisId) {
        thisEl = $('#'+thisId);
        ety = thisEl.attr('data-type');
        ete = thisEl.attr('data-template');
        elArr[thisId] = {
            'type':ety,
            'template':ete,
            'name':$('#'+thisId).text()
        }
        for (var key in myPages[ete]['templates'][ety]['fields']) {
            elArr[thisId][key] = {};
            for (var ikey in myPages[ete]['templates'][ety]['fields'][key]) {
                elArr[thisId][key][ikey] =  myPages[ete]['templates'][ety]['fields'][key][ikey];
            }
        }
    }

    editElement = function (thisId) {
        //If this is the first time we've launched this element, set all the default values
        if (!elArr[thisId]) {
            initialLoad = true;
            prepareElement(thisId);
        } else {
            initialLoad = false;
        }

        previewID = window.pageType+thisId;
        $('#edit-pane').attr('data-id',thisId).fadeIn();
        pString = '<input id="edit-title" type="text" value="'+elArr[thisId]['name']+'"/>';
        for (var key in elArr[thisId]) {
            if (key != 'type' && key != 'template' && key != 'name') {
                pString += '<div class="editSection" id="'+key+'">';
                pString += '<div class="editSectionTitle">'+key+'</div>';
                for (var ikey in elArr[thisId][key]) {
                    if (labels[ikey]) {
                        label = labels[ikey];
                    } else {
                        label = ikey;
                    }

                    is_bool = ($.inArray(ikey,boollist) < 0 ? false : true);

                    if (label != 'coords') {
                        pString += '<label id="l-'+key+'-'+ikey+'" class="'+ikey+(is_bool?' bool':'')+'" for="">'+label+'</label>';
                    }                    
                    if (is_bool) {
                        pString += '<input name="'+key+'-'+ikey+'" id="'+key+'-'+ikey+'" class="'+ikey+'" type="checkbox" '+elArr[thisId][key][ikey]+'/><br/>';
                    } else if (ikey=='margin') {
                        pString += '<div id="marginarea"><input type="checkbox" id="margin1" class="boxmargin" '+(elArr[thisId][key][ikey].substring(0,1)=='1'?'checked':'')+'/>'+
                            '<input type="checkbox" id="margin2" class="boxmargin" '+(elArr[thisId][key][ikey].substring(1,2)=='1'?'checked':'')+'/>'+
                            '<input type="checkbox" id="margin3" class="boxmargin" '+(elArr[thisId][key][ikey].substring(2,3)=='1'?'checked':'')+'/>'+
                            '<input type="checkbox" id="margin4" class="boxmargin" '+(elArr[thisId][key][ikey].substring(3,4)=='1'?'checked':'')+'/>'+
                            '<input type="hidden" name="'+key+'-'+ikey+'" id="'+key+'-'+ikey+'" '+(elArr[thisId][key][ikey].substring(3,4)=='1'?'checked':'')+'/></div>';
                    } else if (ikey=='coords') {
                        pString += '<div id="redraw-coords" class="button">Draw Area</div><input class="'+ikey+'" type="hidden" name="'+key+'-'+ikey+'" id="'+key+'-'+ikey+'" value="'+elArr[thisId][key][ikey]+'" />';
                    } else {
                        pString += '<input name="'+key+'-'+ikey+'" class="'+ikey+'" id="'+key+'-'+ikey+'" type="text" value="'+elArr[thisId][key][ikey]+'"/>';
                    }


                    //Get Preview ID
                    if ((ikey=='id') && (elArr[thisId][key][ikey]!='')) {
                        previewID=elArr[thisId][key][ikey];
                    }
                }
                pString += '</div>';
            }
        }
        pString += '<div class="actions clear">'+
            '<a id="edit-save" href="javascript:" class="button positive">Save</a>'+
            '<a id="edit-cancel" href="javascript:" class="button">Cancel</a>'+
            '<a id="edit-delete" href="javascript:" class="button negative">Delete</a>'+
        '</div><div class="clear"></div>';


        //Highlight this content in the preview
        if (elArr[thisId]['type'] == 'area') {
            pString += '<style>'+
                '#preview-pane #'+previewID+' {background-color:rgba(0,153,255,.5);outline:1px solid #09F;}'
            '</style>';
        } else if (elArr[thisId]['type'] == 'map') {
            pString += '<style>'+
                '#preview-pane #'+previewID+' a {background-color:rgba(0,153,255,.5);outline:1px solid #09F;}'
            '</style>';
        }

        $('#edit-area').html(pString);

        $('#edit-area input').focus(function() {
            $(this).attr('data-init-focus',$(this).val());
        }).blur(function() {
            if ($(this).attr('data-init-focus') == '' && $(this).val() != '') {
                //Set Value and update the preview
                saveValue($(this).attr('id'));
                generateHTML();

                //Animate Flash
                $(this).css('background-color','#0F0').animate({
                    opacity:'1'
                },{
                    duration:'500',
                    progress: function(tot,cur,rem) {
                        hex = Math.round(cur*15).toString(16);
                        $(this).css('background-color','#'+hex+'F'+hex);
                    }
                });
            }
        });
        
        /****** BOOLEAN DAISY CHAINING *******/
        for(var key in parentlist) {
            //TODO: Check if we should hide it.
            $('.'+key).hide();
            curChild = $('.'+parentlist[key]).attr('data-child');
            $('.'+parentlist[key]).attr('data-child',(curChild?curChild+',':'')+key).change(function() {
                childel = $(this).attr('data-child');
                sectionkey = $(this).parents('.editSection').attr('id');
                checked = $(this).is(':checked');
                childels = childel.split(',');
                for (i=0; i<childels.length;i++) {
                    sel = sectionkey+'-'+childels[i];
                    if (checked) {
                        $('#'+sel+', #l-'+sel).fadeIn();
                    } else {
                        $('#'+sel+', #l-'+sel).fadeOut();
                    }
                }
            });
        }

       $('#edit-area input').change(function() {
            $('#edit-area').addClass('edited');
        }).keyup(function() {
            $('#edit-area').addClass('edited');
        });

        $('.actions .button').click(function(){
            tID = $(this).attr('id');
            if (tID=='edit-save') {
                editSave();
            } else if (tID=='edit-cancel') {
                hideEdit();
            } else if (tID=='edit-delete') {
                if (confirm('Are you sure you want to delete this template?')) {
                    editDelete($('#edit-pane').attr('data-id'));
                } else {
                    return false;
                }
            }
        });
        $('#redraw-coords').click(function() {
            imageMapper(thisId);
        });

        if (initialLoad) {
            $('#edit-title').select();
            if ($('#area-coords').length>0) {
                $('#redraw-coords').click();
            }
        }
        

        return true;
    }

    hideEdit = function () {
        $('#edit-area').removeClass('edited');
        if ($('.element-link.active').attr('data-parent')) {
            myTarget = $('.element-link.active').attr('data-parent');
        } else {
            myTarget = '';
        }
        $('#edit-pane').fadeOut(400, function() {setInactive(myTarget)});
        $('#edit-area').html('');
    }

    editSave = function(autoClose) {
        /* Title */
        el = $('#edit-pane').attr('data-id');
        $('#'+el).text($('#edit-title').val());

        marginvar = '';
        $('#edit-area input').each(function() {
            saveValue($(this).attr('id'));
        });

        $('#edit-area').removeClass('edited');
        if(autoClose != false) {
            hideEdit();
        }
        generateHTML();
    }

    saveValue = function (elid) {
        el = $('#edit-pane').attr('data-id');
        $('#'+elid).each(function() {
            if ($(this).attr('id') == 'edit-title') {
                elArr[el]['name'] = $(this).val();
            } else {
                keys = $(this).attr('id');
                keys = keys.split('-');
                if ($(this).attr('type') == 'checkbox') {
                    if ($(this).hasClass('boxmargin')) {
                        marginvar += ($(this).is(':checked')?'1':'0');
                    } else {
                        if ($(this).is(':checked')) {
                            elArr[el][keys[0]][keys[1]] = 'checked';
                        } else {
                            elArr[el][keys[0]][keys[1]] = '';
                        }
                    }
                } else if ($(this).attr('id').match('margin')) {
                    elArr[el][keys[0]][keys[1]] = marginvar;
                } else {
                    elArr[el][keys[0]][keys[1]] = $(this).val().replace(/http:\/\/(www\.|staging2\.|prod-www\.|preview-prod-www\.){0,1}express.com/,'');
                }
            }
        });
    }

    editDelete = function (el) {
        //If this is a parent element
        if ($('#'+el).hasClass('pre')) {
            myParent = $('#'+el).parent();
            $('#'+el).parent().find('.element-link').each(function() {
                //elArr.splice($(this).attr('id'),1);
                $(this).remove();
            });

            myParent.remove();
        } else {
            //elArr.splice(el,1);
            $('#'+el).remove();
        }
        
        hideEdit();
        generateHTML();
    }


    /****************** TEMPLATE SPECIFIC FUNCTIONS *****************/
    $('#map-cancel, #map-accept').click(function() {
        if ($(this).attr('id')=='map-accept') {
            $('#edit-title').select();
            if ($('#area-coords').val() == '') {
                $('#area-coords').val(window.currentCoords);
                saveValue('area-coords');
                generateHTML();
            } else {
                $('#area-coords').val(window.currentCoords);
            }
        }
        $('#mapel').imgAreaSelect({'remove': true});
        $('#image-mapper').fadeOut();
    });
    imageMapper = function (curArea) {
        // Getting current image
        curMap = $('#'+curArea).attr('data-parent');
        imgURL = elArr[curMap]['image']['src'];
        if (imgURL.match(/^\//)) {
            imgURL = 'http://www.express.com'+imgURL;
        }

        //Kill the call if we don't have an image
        if (imgURL == '') {
            return false;
        }

        $('#image-mapper').fadeIn();
        $('#image-mapper-image').html('<img id="mapel" src="'+imgURL+'" alt="Map This Element" />');
        
        if ($('#area-coords').val() != '') {
            //Recreate the previous selection as well.
            c = $('#area-coords').val().split(',');
            $('#mapel').imgAreaSelect({
                handles: true,
                x1:parseInt(c[0]),
                y1:parseInt(c[1]),
                x2:(parseInt(c[0])+parseInt(c[2])),
                y2:(parseInt(c[1])+parseInt(c[3])),
                height:parseInt(c[3]),
                width:parseInt(c[2]),
                parent:'#image-mapper-image',
                onSelectEnd: function (img, selection) {
                    window.currentCoords = selection.x1 + ','+ selection.y1 + ',' + selection.width + ',' + selection.height;
                }
            });
        } else {
            //Initial Plotting
            $('#mapel').imgAreaSelect({
                handles: true,
                parent:'#image-mapper-image',
                onSelectEnd: function (img, selection) {
                    window.currentCoords = selection.x1 + ','+ selection.y1 + ',' + selection.width + ',' + selection.height;
                }
            });
        }
    }

    submitCoords = function() {
        alert('selected area');
    }
});