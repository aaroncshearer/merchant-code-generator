/***************** GENERATE HTML ******************/
generateHTML = function () {
    page = $('#new-page-selector').val();
    /*Insert page template*/
    pString = myPages[page]['pre'];
    classArr = ['padl','padt','padr','padb'];

    $('.element-link').each(function() {
        tID = $(this).attr('id');

        if (!(tTemplate = elArr[tID])) {
            //This is a post el, check the previous new element for the pre
            tID = parseInt(tID.match(/\d+$/)[0]);
            tID --;
            tID = 'el-'+tID;
            tTemplate = elArr[tID]['template'];
        }
        tTemplate = elArr[tID]['template'];
        tType = elArr[tID]['type'];

        if ($(this).hasClass('pre')) {
           temp = myPages[tTemplate]['templates'][tType]['pre'] + '';
        } else if ($(this).hasClass('post')) {
           temp = myPages[tTemplate]['templates'][tType]['post'] + '';
        } else {
            temp = myPages[tTemplate]['templates'][tType]['html'] + '';
        }

        /*insert element template */
        if (elArr[tID]['link']) {
            if (elArr[tID]['link']['href']=="") {
                temp = myPages[tTemplate]['templates'][tType]['html'].replace(/\<a.*?\<img/,'<img class="fl"').replace(/\<\/a\>/,'');
            }
        }

        for (key in elArr[tID]) {
            if (key != 'template' && key != 'type' && key != 'name') {
                for(ikey in elArr[tID][key]) {
                    value = elArr[tID][key][ikey];
                    if (ikey=='target' && value=='checked') {
                        //Handle "Open in new window checkbox"
                        value = ' target="_blank"';
                    } else if (ikey=='margin') {
                        //Handle spacing Checkboxes
                        if (value != '0000') {
                            marginArr = value.split('');
                            value = '';
                            for(i=0;i<marginArr.length;i++) {
                                value += marginArr[i]=='1' ? classArr[i]+' ' : '';
                            }
                            if (classMatch = temp.match(/class=".*?"/)) {
                                classMatch = classMatch[0];
                                classMatch = classMatch.substring(0,classMatch.length-1);
                                temp = temp.replace(new RegExp('class=".*?"'),classMatch+' '+value+'"');
                            }
                            
                        }
                    } else if (ikey=='height' || ikey=='width') {
                        //Insert "px" if there is no units
                        value = (value.match(/(px|em|\%)/)?value:value+'px');
                    } else if (ikey=='coords') {
                        c = value.split(',');
                        //__areax1__
                        temp = temp.replace('__areax1__','left:'+c[0]+'px;');
                        temp = temp.replace('__areay1__','top:'+c[1]+'px;');
                        temp = temp.replace('__areax2__','width:'+c[2]+'px;');
                        temp = temp.replace('__areay2__','height:'+c[3]+'px;');
                    } else if (ikey=='id') {
                        if (value=='') {
                            value = window.pageType+tID;
                        }
                    } else if (ikey=='popup' && value=='checked') {
                        tempid = elArr[tID][key]['id']==''?window.pageType+tID:elArr[tID][key]['id'];
                        temp += '<script type="text/javascript">exp.utils.popupLink("#'+(tempid)+'",{width:'+elArr[tID][key]['popwidth']+',height:'+elArr[tID][key]['popheight']+'});</script>';
                    } else if (ikey=='isVid') {
                        if(value=='checked'){
                            value = ' videoLink';
                        } else {
                            value = '';
                        }
                    } if (ikey=='href' && value=='') {
                        //Removing anchors on blank links
                        temp = temp.replace(new RegExp('\<\/{0,1}a.*?\>', 'g'),'');
                    }
                    temp = temp.replace(new RegExp('__'+key+ikey+'__', 'g'),value);
                }
            }
        }

        pString += temp;
    });
    pString += myPages[page]['post'];

    /*************** HANDLE RELATIVE PATHS ***************/
    pString = pString.replace(/src="\//g,'src="http://www.express.com/');
    pString = pString.replace(/href="\//g,'href="http://www.express.com/');

    $('#preview-pane .preview').html(pString);
    $('#checkLinks').text('Check Links').removeClass('positive').removeClass('negative');

    if ($('#checkLinks:visible, #export:visible').length == 0) {
        aniArr = ['#checkLinks','#export'];
        for(i=0;i<aniArr.length;i++) {
            $(aniArr[i]).css({
                'width':'0',
                'padding-left':'0',
                'padding-right':'0',
                'display':'inline-block',
                'opacity':'0'
            }).animate({
                'width':$(aniArr[i]).attr('data-width')+'px',
                'padding-left':'5px',
                'padding-right':'5px',
                'opacity':'1'
            },1000, function() {
                $(this).css('width','auto');
            });
        }
    }

    $('#preview-pane').css('background','#DDD');
    pageEdited = true;
}

pageEdited = false;
window.onbeforeunload = function() {
    if (pageEdited) {
        if (! confirm('Are you sure you want to leave?')) {
            return 'You have edited content without submitting.';
        }
    }
}

/***************** CHECK LINKS FUNCTION ***********/
checkLinks = function(myLinks,iterator) {
    window.iterator = iterator;
    window.myLinks = myLinks;
    if (myLinks[iterator]) {
        if (myLinks[iterator].match(/^\//)) {
            myLinks[iterator] = 'http://www.express.com'+myLinks[iterator];
        }
        $('#checkLinks').text('Checking '+(iterator+1)+'/'+myLinks.length);
        $('.fourohfour').removeClass('fourohfour');
        $('.crossdomain').removeClass('crossdomain');

        $.ajax({
            type: 'get',
            url: myLinks[iterator],
            success: function(data, textStatus, XMLHttpRequest){
                /*console.log('d:'+data);
                console.log('t:'+textStatus);
                console.log('x:'+XMLHttpRequest);*/
                checkLinks(window.myLinks,window.iterator+1);
            },
            error:function (xhr, ajaxOptions, thrownError){
                if (xhr.status == 0) { //Most likely Access-Control-Allow-Origin
                    window.crossD[window.crossD.length]=(window.iterator);
                } else {
                    window.log404[window.log404.length]=(window.iterator);
                }
                checkLinks(window.myLinks,window.iterator+1);
            }
        });
    } else {
        errLog = 'There were '+(window.log404.length > 0 ? window.log404.length + ' links that don\'t exist':'')+(window.log404.length>0 && window.crossD.length >0?' and':'')+(window.crossD.length>0 ? window.crossD.length + ' links that are not on this domain':'')+'.';
        for (i=0;i<window.log404.length;i++) {
            $('#'+myLinkNames[window.log404[i]]).addClass('fourohfour');
        }
        for (i=0;i<window.crossD.length;i++) {
            $('#'+myLinkNames[window.crossD[i]]).addClass('crossdomain');
        }


        if (errLog != '') {
            $('#checkLinks').text('Error').addClass('negative');
            //Delay this for just a little so the images pop in
            setTimeout(function(){alert(errLog)},500);                
        } else {
            $('#checkLinks').text('Success').addClass('positive');
        }
    }
}
$(document).ready(function() {
    $('#checkLinks').click(function() {
        $('#checkLinks').removeClass('positive').removeClass('negative');
        window.log404 = new Array();
        window.crossD = new Array();
        myString = $('#preview-pane').html();
        if (myString.match(/href=".+?"/g)) {
            myLinks = new Array();
            myLinkNames = new Array();

            $('.element-link').each(function() {
                tID = $(this).attr('id');

                for (key in elArr[tID]) {
                    if (elArr[tID][key]['href']) {
                        myLinks[myLinks.length]=(elArr[tID][key]['href']);
                        myLinkNames[myLinkNames.length]=tID;
                    }
                }
            });
            checkLinks(myLinks,0);
        } else {
            $('#checkLinks').text('No Links Found');
        }
    });
});

/***************** IMPORT FUNCTION ****************/
$(document).ready(function() {
    modalVisible = false;

    $('#import').click(function() {
        $('#load-modal').html('<div class="inner-modal"><iframe id="browser" src="browse.php" /></div>').fadeIn(400,function() {modalVisible=true;});
    });

    $('html').click(function() {
        if (modalVisible == true) {
            modalVisible = false;
            $('#load-modal').fadeOut();
        } 
    });

    $('.inner-modal').click(function(event){
        event.stopPropagation();
    });
});

importfile = function(filename) {
    prevFileName = filename.replace(/.*\//g,''); //This is just for auto filling the name in when the try to resave
    
    curloc = window.location.pathname;
    fileloc = curloc + filename;
    d = Math.round(new Date().getTime() / 1000);
    fileloc = fileloc.replace(/\/\/+/g,'/').replace('http:/','http://') + '?'+ d;
    
    modalVisible = false;
    $('#load-modal').fadeOut();

    $.ajax({
        type: 'get',
        url: fileloc,
        success: function(data){
            keys = data.split(/[\r\n]+/);
            resetVars();
            window.pageType = stripslashes(keys[0]);
            myHTML = stripslashes(keys[2]);

            //Set Up Listeners
            $('#new-page-selector').val(keys[0]).change();

            elArr = {};
            elArr = jQuery.parseJSON(stripslashes(keys[1]));

            $('#element-list').html(myHTML).sortable({ distance: 15, update: function() {generateHTML();} });

            $('.element-link').focus(function() {
                setInactive($(this).attr('id'));
            }).click(function(){
                setInactive($(this).attr('id'));
            }).dblclick(function() {
                curname = $(this).text();
                if (ren = prompt("Rename Template\r\n\r\nEnter New Template Name:",curname)) {
                    $(this).text(ren);
                    $('#edit-title').val(ren);
                    editSave(false);
                }
            });

            $('.element-link').parent().find('.children').sortable({ distance: 15, update: function() {generateHTML();} });

            $('.add-sub-element-link').click(function() {
                addEl($(this).attr('id'));
            });

            window.numElements = 0;
            $('.element-link').each(function() {
                thisID = parseInt($(this).attr('id').replace(/[^\d]+/g,''));
                if (thisID>window.numElements) {
                    window.numElements = thisID;
                }
            });
            window.numElements ++;            

            setInactive('');
            generateHTML();

            pageEdited = false;
        },
        error:function() {
            alert('Error obtaining contents of '+fileloc);
        }
    });
}

/********************** EXPORT FUNCTION ****************/
$(document).ready(function() {
    prevFileName = '';
    $('#export').click(function() {
        prevFileName = prevFileName.replace(/\..*?$/,'');
        if (filename = prompt('What would you like to name the file?'+"\r\n(If it already exists, it will be overwritten)",prevFileName)) {
            if (filename == '') {
                alert("You must include a file name.");
                return false;
            }
            
            nfilename = filename.replace(/\..+/,'');
            nfilename = nfilename.replace(/[^a-zA-Z0-9\-\_]+/g,'');

            if ((nfilename+'.txt' != filename) && (nfilename != filename)) {
                alert('The file you are submitting has been renamed '+nfilename+'.txt');
            }

            var json_text = JSON.stringify(elArr, null, 2).replace(/[\r\n]+/g,'').replace(/\s\s+/g,' ');
            var html_text = $('#element-list')[0].innerHTML.replace(/[\r\n]+/g,'').replace(/\s\s+/g,' ');
            var export_html = $('#preview-pane .preview')[0].innerHTML.replace(/http:\/\/(www\.|staging2\.|prod-www\.|preview-prod-www\.){0,1}express.com/g,'');

            $.ajax({
                type: "POST",
                url: 'submit.php',
                data: {
                    'pagetype':window.pageType,
                    'leftnav':html_text,
                    'fname':nfilename,
                    'json':json_text,
                    'html':export_html
                },
                success: function(data, textStatus, XMLHttpRequest){
                    if (data.match(/success/)) {
                        alert('Submission successful');
                        pageEdited = false;
                    } else {
                        alert(data);
                    }
                },
                error:function (xhr, ajaxOptions, thrownError) {
                    alert('Error submitting to file');
                }
            });
        }
    });
});

function stripslashes (str) {
  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    default:
      return n1;
    }
  });
}