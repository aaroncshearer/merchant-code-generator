var tver = '0.1.13';
var elArr = {};
var myPages = {
    'dhp':{'name':'Desktop Home','moreTemplates':'dglobal',
        'pre':'<link href="/cdn/assets/cms/css/dlp.css" rel="stylesheet" type="text/css" media="screen, print"/><div style="width:946px">',
        'post':'</div><div class="cl"></div>'},
    'dwlp':{'name':'Desktop Women\'s Landing','moreTemplates':'dglobal',
        'pre':'<link href="/cdn/assets/cms/css/dlp.css" rel="stylesheet" type="text/css" media="screen, print"/><div style="width:790px">',
        'post':'</div>'},
    'dmlp':{'name':'Desktop Men\'s Landing','moreTemplates':'dglobal',
        'pre':'<link href="/cdn/assets/cms/css/dlp.css" rel="stylesheet" type="text/css" media="screen, print"/><div style="width:790px">',
        'post':'</div>'},
    'dglb':{'name':'Desktop Global Banner','moreTemplates':'dglobal',
        'pre':'<link href="/cdn/assets/cms/css/dlp.css" rel="stylesheet" type="text/css" media="screen, print"/> <div class="global-banner">',
        'post':'</div><style>.global-banner{background:white; padding: 0 12px;}</style>'},
    'mhp':{'name':'Mobile Home','moreTemplates':'mglobal',
        'pre':'<link href="/cdn/assets/cms/css/mlp.css" rel="stylesheet" type="text/css" media="screen, print"/><div id="container">',
        'post':'</div>'},
    'mwlp':{'name':'Mobile Women\'s Landing','moreTemplates':'mglobal',
        'pre':'<link href="/cdn/assets/cms/css/mlp.css" rel="stylesheet" type="text/css" media="screen, print"/><div id="container">',
        'post':'</div>'},
    'mmlp':{'name':'Mobile Men\'s Landing','moreTemplates':'mglobal',
        'pre':'<link href="/cdn/assets/cms/css/mlp.css" rel="stylesheet" type="text/css" media="screen, print"/><div id="container">',
        'post':'</div>'},
};

//Defining Global Templates That aren't selectable
myPages['dglobal'] = {}; 
myPages['dlp'] = {};
myPages['mglobal'] = {};

/***************************** TEMPLATES **********************/
myPages['dglobal']['templates'] = {
    'simple':
        {'name':'Simple Image',
         'html':'<a id="__imageid__" class="fl" href="__linkhref__"__linktarget__><image class="fl__linkisVid__" src="__imagesrc__" alt="__imagealt__" /></a>',
         'fields':{
            'image':{'id':'','src':'','alt':''},
            'link':{'href':'','target':'','popup':'','popwidth':'640','popheight':'250','isVid':'','margin':''}
        }},
    'map':
        {'name':'Image Map',
         'pre':'<div class="map fl" id="__mapid__"><image src="__imagesrc__" alt="__imagealt__"/>',
         'post':'</div>',
         'children':['dglobal-area'],
         'fields':{
            'image':{'src':'','alt':'','margin':''},
            'map':{'id':''}
        }},
    'area':
        {'name':'Area',
         'requiresparent':true,
         'html':'<a id="__areaid__" style="__areax1____areay1____areax2____areay2__" href="__areahref__" __areatarget__>__areadesc__</a>',
         'fields':{
            'area':{'href':'','desc':'','target':'','popup':'','popwidth':'640','popheight':'250','isVid':'','coords':'','id':''}
        }},
    'carousel':
        {'name':'Carousel',
         'debug':true,
         'html':'Blah',
         'children':['dglobal-simple','dglobal-map'],
         'fields':{
            'settings':{'autoplay':'','aptime':'5','loop':'','dots':''}
        }},
    'flipper':
        {'name':'Flipper',
         'debug':true,
         'html':'Blah',
         'children':['dglobal-simple'],
         'fields':{
            'settings':{'fliptime':'5','hoverpause':'checked'}
        }},
    'videoSettings':
        {'name':'Video Settings',
         // 'requiresparent':true,
         'html':'<script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>'+
                '<script type="text/javascript">'+
                'mycontent = \'<div style="background-color:#000;width:__videorawWidth__px;height:__videorawHeight__px;"><object id="myExperience__videovidId__" class="BrightcoveExperience">'+
                  '<param name="bgcolor" value="#FFFFFF" />'+
                  '<param name="width" value="__videorawWidth__" />'+
                  '<param name="height" value="__videorawHeight__" />'+
                  '<param name="wmode" value="transparent" />'+
                  '<param name="playerID" value="1764129607001" />'+
                  '<param name="playerKey" value="AQ~~,AAAAA1_u5pE~,sCjkA1iBn1aw0TWdIdG-2UQYqGPgTMIv" />'+
                  '<param name="isVid" value="true" />'+
                  '<param name="isUI" value="true" />'+
                  '<param name="dynamicStreaming" value="true" />'+
                  '<param name="includeAPI" value="true" />  '+
                  '<param name="autoStart" value="true" />  '+
                  '<param name="templateLoadHandler" value="myTemplateLoaded" /> '+
                  '<param name="@videoPlayer" value="__videovidId__" />'+
                '</object></div>\';'+

                '$(document).ready(function() {'+
                    '$(".videoLink").fancybox({'+
                    '"titleShow"         : false,'+
                    '"padding"           : 0,'+
                    '"width"             : __videorawWidth__,'+
                    '"height"            : __videorawHeight__,'+
                    '"scrolling"         : "no",'+
                    '"centerOnScroll"    : true,'+
                    '"autoScale"         : false,'+
                    '"transitionIn"      : "fade",'+
                    '"transitionOut"     : "fade",'+
                    '"content"           : mycontent'+
                '}).click(function(e) {'+
                    'brightcove.createExperiences();'+
                    'e.preventDefault();'+
                '});'+

                '});'+
                '</script>',
         'fields':{
            'video':{'vidId':'','rawWidth':'814','rawHeight':'457'}
        }},
    'FPO':{'name':'For Position Only',
         'html':'<div class="cl fl" style="width:__settingswidth__; height:__settingsheight__; line-height:__settingsheight__; width:100%; background:#6DF; color:#FFF; font-size:28px; font-weight:bold; text-align:center; overflow:hidden;">FPO</div>',
         'fields':{
            'settings':{'height':'','width':''}
        }}
};

myPages['mglobal']['templates'] = {
    'simple':
        {'name':'Simple Image',
         'html':'<a id="__imageid__" href="__linkhref__"__linktarget__><image src="__imagesrc__" alt="__imagealt__" /></a>',
         'fields':{
            'image':{'id':'','src':'','alt':''},
            'link':{'href':'','target':''}
        }},
    'map':
        {'name':'Image Map',
         'pre':'<div class="map fl" id="__mapid__"><image src="__imagesrc__" alt="__imagealt__"/>',
         'post':'</div>',
         'children':['dglobal-area'],
         'fields':{
            'image':{'src':'','alt':'','margin':''},
            'map':{'id':''}
        }},
    'area':
        {'name':'Area',
         'requiresparent':true,
         'html':'<a id="__areaid__" style="__areax1____areay1____areax2____areay2__" href="__areahref__" __areatarget__>__areadesc__</a>',
         'fields':{
            'area':{'href':'','desc':'','target':'','popup':'','popwidth':'640','popheight':'250','coords':'','id':''}
        }},
    'carousel':
        {'name':'Carousel',
         'debug':true,
         'html':'Blah',
         'children':['dglobal-simple','dglobal-map'],
         'fields':{
            'settings':{'autoplay':'','aptime':'5','loop':'','dots':''}
        }},
    'FPO':{'name':'For Position Only',
         'html':'<div class="cl fl" style="width: 100%; height:__settingsheight__; line-height:__settingsheight__; width:100%; background:#6DF; color:#FFF; font-size:28px; font-weight:bold; text-align:center; overflow:hidden;">FPO</div>',
         'fields':{
            'settings':{'height':''}
        }}
}

/************** FIELD DESCRIPTIONS ************/
labels = {
    'src':'Image Source',
    'alt':'Image Description',
    'href':'Link Location',
    'target':'Open in new Window?',
    'margin':'Spacing',
    'desc':'Description',
    'autoplay':'Autoplay?',
    'aptime':'Autoplay Time <small>(seconds)</small>',
    'loop':'Loop carousel?',
    'dots':'Show page indicators?',
    'fliptime':'Slide delay time <small>(seconds)</small>',
    'hoverpause':'Pause on hover?',
    'popup':'Open in a popup?',
    'popwidth':'Popup Width <small>(pixels)</smalL>',
    'popheight':'Popup Height <small>(pixels)</smalL>',
    'vidId':'Video ID',
    'rawWidth':'Video Width <small>(pixels)</small>',
    'rawHeight':'Video Height <small>(pixels)</small>',
    'isVid':'Link to Video?'
}

boollist = ['target','autoplay','loop','dots','hoverpause','popup','isVid'];
parentlist = {
    'aptime':'autoplay',
    'popwidth':'popup',
    'popheight':'popup'
}
