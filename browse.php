<?php

	// RTroyer: This is a very simple file browser, designed for lightweight implementation
	echo "<html><head><title>File Browse</title><script src=\"scripts/jquery-1.9.1.js\" type=\"text/javascript\"></script>";
	?>
	
	<link href="styles/application.css" rel="stylesheet">
	
	<?php
	echo "</head><body>";
	
	$extArr = array(".txt");

	//Functions
	function curPageURL($strip) {
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
			if ($strip) 
				$pageURL .= $_SERVER["PHP_SELF"];
			else
				$pageURL .= $_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"];
			if ($strip) 
				$pageURL .= $_SERVER["PHP_SELF"];
			else
				$pageURL .= $_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}

	$loadedfolder=$_GET['f'];
	$loadedfolder = preg_replace('/(\/|^)[^\/]*?\/\.\.\//', '/', $loadedfolder);
	
	$folderArr = preg_split("/\//", $loadedfolder);
	$loadedfolder = '';
	for ($i=0;$i<count($folderArr);$i++) {
		if ($folderArr[$i] <> '')
			$loadedfolder .= $folderArr[$i].'/';
	}

	$y = date('Y');
    $m = date('m');
    $d = date('d');
    
	$defauldir = 'uploads/';
	if ($loadedfolder <> '') {
		$folder=dir($defauldir.$loadedfolder);  
	} else {
		if (!is_dir ($defauldir.$y)) {mkdir($defauldir.$y);}
    	if (!is_dir ($defauldir.$y.'/'.$m)) {mkdir($defauldir.$y.'/'.$m);}
		$folder=dir($defauldir.$y.'/'.$m);
		$folderArr[0] = $y;
		$folderArr[1] = $m;
		$loadedfolder = $y.'/'.$m.'/';
	}
	echo '<div class="titlebar"><h1>Browse</h1>';

	$folderArr = preg_split("/\//", $loadedfolder);
	if ($loadedfolder <> "" && $loadedfolder <> './') {
		echo '<a class="upfolder" href="'.curPageURL(true).'?f=';
		$len = count($folderArr)-2;
		if ($len<=0) {
			echo './';
		} else {
			for ($i=0;$i<$len;$i++) {
				if (($folderArr[$i]<>'') && ($folderArr[$i]<>'.')) {
					echo $folderArr[$i].'/';
				}
			}
		}
		echo '">Up A Folder</a>';
	}

	echo '<div class="folderarea"'.($loadedfolder<>'' && $loadedfolder<>'./' ?' style="width:268px"':'').'>';
	$urlString = '';
	echo '<a href="?f=./">Uploads</a> / ';
	for ($i=0;$i<count($folderArr);$i++) {
		if (($folderArr[$i]<>'') && ($folderArr[$i]<>'.')) {
			$urlString .= $folderArr[$i].'/';
			echo '<a href="?f='.$urlString.'">'.$folderArr[$i].'</a> / ';
		}
	}
	echo '</div></div>';
	

	echo '<div id="browse-folder">';
	$tempArr = array();
	while ($folderEntry=$folder->read()) {
		if ($folderEntry != ".." xor $folderEntry != ".") {
		
		} else{
			$file = $loadedfolder . $folderEntry;

			if (preg_match('/\..*?$/', $folderEntry, $ext)) {
				$ext = strtolower($ext[0]);
			} else {
				$ext = '';
			}

			if ($ext=='') {
				$tempArr[count($tempArr)] = "<a class=\"folder\" href=\"".curPageURL(true)."?f=".$loadedfolder . $folderEntry."/\">".$folderEntry."</a>";
			} else {
				if (in_array($ext, $extArr)) {
					$tempArr[count($tempArr)] = '<a class="file" data-file="/'.$defauldir.$loadedfolder.$folderEntry.'" href="javascript:">'.preg_replace('/\..*?$/', '',$folderEntry).'</a>';
				}
			}
		}
	}
	
	$folder->close();

	sort($tempArr);
	for ($i=0;$i<count($tempArr);$i++) {
		echo $tempArr[$i];
	}

	if ($loadedfolder == "") {
		$loadedfolder = ".";
	}
	
	if (count($tempArr) == 0) {
		echo '<div class="empty-directory">This directory is empty.</div>';
	}

	echo '</div>';

	?>



	<script>
		$(document).ready(function() {
			$('.file').click(function() {
				if (parent.importfile) {
					parent.importfile($(this).attr('data-file'));
				} else {
					alert('Please import in the main window.');
				}
			});
		});
	</script>
	
	<?php
	echo "</body></html>";
?>