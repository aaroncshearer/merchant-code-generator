<?php
    $y = date('Y');
    $m = date('m');
    $d = date('d');
    $eName = 'uploads/'.$y.'/'.$m.'/'.stripslashes($_POST['fname']).'.txt';
    $hName = 'uploads/'.$y.'/'.$m.'/'.$y.$m.$d.'-'.stripslashes($_POST['fname']).'.html';
    $pageType = stripslashes($_POST['pagetype']);
    $myJSON = stripslashes($_POST['json']);
    $leftnav = stripslashes($_POST['leftnav']);
    $myHTML = stripslashes($_POST['html']);

    if (!is_dir ('uploads/'.$y)) {mkdir('uploads/'.$y);}
    if (!is_dir ('uploads/'.$y.'/'.$m)) {mkdir('uploads/'.$y.'/'.$m);}

    header("Content-Type: text/plain");
    if (!$handle = fopen($eName, 'w')) {
        echo 'cannot open '.$filename;
        exit;
    }

    if (fwrite($handle, $pageType."\n".$myJSON."\n".$leftnav) === FALSE) {
        echo 'cannot write to '.$filename;
        exit;
    }
    fclose($handle);


    if (!$handle = fopen($hName, 'w')) {
        echo 'cannot open '.$filename;
        exit;
    }
    if (fwrite($handle, $myHTML) === FALSE) {
        echo 'cannot write to '.$filename;
        exit;
    }
    echo 'success';
    fclose($handle);

?>